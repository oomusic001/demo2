//package com.example.redis.serializer;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.connection.DefaultStringRedisConnection;
//import org.springframework.data.redis.connection.RedisConnection;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.serializer.RedisSerializer;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//import org.springframework.stereotype.Service;
//
//@Service
//public class FastJsonRedisTemplate extends RedisTemplate<String ,String> {
//
//    @Autowired
//    private RedisConnectionFactory redisConnectionFactory;
//
//    public FastJsonRedisTemplate() {
//        RedisSerializer<String> stringSerializer = new StringRedisSerializer();
//        FastJsonRedisSerializer redisSerializer = new FastJsonRedisSerializer();
//        this.setKeySerializer(stringSerializer);//设置key的序列化方式
//        this.setValueSerializer(redisSerializer);//设置value的序列化方式
//        this.setHashKeySerializer(stringSerializer);
//        this.setHashValueSerializer(redisSerializer);
//    }
//
//    /**
//     * 设置序列化方式。
//     * 设置连接工厂。
//     * @param connectionFactory
//     */
//    public FastJsonRedisTemplate(RedisConnectionFactory connectionFactory){
//        this();
//        this.setConnectionFactory(connectionFactory);
//        this.afterPropertiesSet();
//    }
//
//    @Override
//    protected RedisConnection preProcessConnection(RedisConnection connection, boolean existingConnection) {
//
//        return new DefaultStringRedisConnection(connection);
//
//    }
//}
