//package com.example.redis.serializer;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.parser.Feature;
//import com.alibaba.fastjson.serializer.SerializerFeature;
//import org.springframework.data.redis.serializer.RedisSerializer;
//import org.springframework.data.redis.serializer.SerializationException;
//
//public class FastJsonRedisSerializer implements RedisSerializer {
//    @Override
//    public byte[] serialize(Object o) throws SerializationException {
//        return o == null ? null : JSON.toJSONBytes(o,new SerializerFeature[]{
//                SerializerFeature.WriteNullStringAsEmpty,
//                SerializerFeature.WriteMapNullValue,
//                SerializerFeature.PrettyFormat,
//                SerializerFeature.WriteDateUseDateFormat,
//                SerializerFeature.WriteNullBooleanAsFalse,
//                SerializerFeature.WriteNullNumberAsZero,
//                SerializerFeature.WriteClassName,
//                SerializerFeature.WriteNullListAsEmpty});
//    }
//
//    @Override
//    public Object deserialize(byte[] bytes) throws SerializationException {
//        return bytes == null?null:JSON.parse(bytes,new Feature[0]);
//    }
//}
