package com.example.redis.pipeline;

import org.springframework.data.redis.core.ZSetOperations;

import java.util.Map;
import java.util.Set;

public interface IZSet extends IPipeline {
    public Set<ZSetOperations.TypedTuple<Object>> getZSet(String key);
    public int addZSet(String key,Set<ZSetOperations.TypedTuple<Object>> values);
    public int delZSet(String key);
    public int addZSetBatch(Map<String,Map<String,Double>> zSets);
}
