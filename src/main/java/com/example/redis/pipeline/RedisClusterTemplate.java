package com.example.redis.pipeline;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;

public class RedisClusterTemplate {
    @Autowired
    RedisTemplate redisTemplate;

    ZSetOperations zSetOperations = redisTemplate.opsForZSet();
    SetOperations setOperations = redisTemplate.opsForSet();
    ValueOperations valueOperations = redisTemplate.opsForValue();
    ListOperations listOperations = redisTemplate.opsForList();
    HashOperations hashOperations = redisTemplate.opsForHash();


}
