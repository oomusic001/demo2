package com.example.redis.pipeline;

import org.springframework.data.redis.core.ZSetOperations;

import java.util.Map;
import java.util.Set;

public interface ISet extends IPipeline {
    public Set<Object> getSet(String key);
    public Long addSet(String key,Set<Object> set);
    public Long delSet(String key);
    public Long addSetBatch(Map<String,Set<String>> sets);
}
