package com.example.redis.pipeline;

import com.example.redis.pipeline.operation.SetOperation;
import com.example.redis.pipeline.operation.ZSetOperation;
import org.springframework.stereotype.Component;
import redis.clients.jedis.*;
import redis.clients.jedis.util.JedisClusterCRC16;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@Component
public class PipelineTemplate extends AbstractPipeline{
    private ZSetOperation zSetOperation;
    private SetOperation setOperation;




    JedisClusterPipeline jedisClusterPlus;
    JedisSlotAdvancedConnectionHandler jedisSlotAdvancedConnectionHandler;



    /**
     * 添加value
     * @param key
     * @param value
     */
    public void addValue(String key,String value){

        Pipeline pipelined = getPipeline(key);
        pipelined.set(key,value);
        pipelined.close();
    }

    public Pipeline getPipeline(String key){
        int slot = JedisClusterCRC16.getSlot(key);
        JedisPool jedisPool = jedisSlotAdvancedConnectionHandler.getJedisPoolFromSlot(slot);
        Jedis jedis = jedisPool.getResource();
        return jedis.pipelined();
    }
    public Jedis getJedis(String key){
        int slot = JedisClusterCRC16.getSlot(key);
        JedisPool jedisPool = jedisSlotAdvancedConnectionHandler.getJedisPoolFromSlot(slot);
        return jedisPool.getResource();
    }
/*********删除bigkey

 1.下面操作可以使用pipeline加速。

 2.redis 4.0已经支持key的异步删除，欢迎使用。************/

    /**
     * Hash删除: hscan + hdel
     * 删除hbigKey
     * @param  bigKey cluster集群情况下 jedisPool有要求，必须根据bigKey计算slot结果而来。
     * @param bigKey
     */
    public void delBigKey( String bigKey){
        Jedis jedis = getJedis(bigKey);
        ScanParams scanParams = new ScanParams().count(100);
        String cursor = "0";
        do {
            ScanResult<Map.Entry<String,String>> scanResult =
                    jedis.hscan(bigKey,cursor,scanParams);
            List<Map.Entry<String, String>> result = scanResult.getResult();
            if (result != null && !result.isEmpty()){
                for (Map.Entry<String, String> entry : result) {
                    jedis.hdel(bigKey,entry.getKey());
                }
            }
            cursor = scanResult.getCursor();
        }while (!"0".equals(cursor));
        jedis.del(bigKey);
    }

    /**
     * List删除: ltrim
     * @param bigListKey
     */
    public void delBigList(String bigListKey){
        Jedis jedis = getJedis(bigListKey);
        Long llen = jedis.llen(bigListKey);
        int counter = 0;
        int left = 1000;
        while (counter<llen){
            //每次从左侧截掉100个
            jedis.ltrim(bigListKey,left,llen);
        }
        //删除key
        jedis.del(bigListKey);
    }

    /**
     * Set删除: sscan + srem
     * @param key
     */
    public void delBigSet(String key){
        Jedis jedis = getJedis(key);
        ScanParams scanParams = new ScanParams().count(100);
        String cursor = "0";
        do {
            ScanResult<String> scanResult = jedis.sscan(key,cursor,scanParams);
            List<String> result = scanResult.getResult();
            if (result!= null && !result.isEmpty()){
                for (String member : result) {
                    jedis.srem(key,member);
                }
            }
            cursor = scanResult.getCursor();
        }while (!"0".equals(cursor));
        //删除key
        jedis.del(key);
    }

    /**
     * SortedSet删除: zscan + zrem
     * @param key
     */
    public void delBigZSet(String key){
        Jedis jedis = getJedis(key);
        ScanParams scanParams = new ScanParams().count(100);
        String cursor = "0";
        do {
            ScanResult<Tuple> scanResult = jedis.zscan(key, cursor, scanParams);
            List<Tuple> result = scanResult.getResult();
            if (result != null && !result.isEmpty()){
                for (Tuple tuple : result) {
                    jedis.zrem(key,tuple.getElement());
                }
            }
            cursor = scanResult.getCursor();
        }while (!"0".equals(cursor));
        //删除key
        jedis.del(key);
    }
}
