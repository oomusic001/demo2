package com.example.redis.pipeline;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.Set;

public class JedisClusterPipeline  extends JedisCluster {

        public JedisClusterPipeline(Set<HostAndPort> jedisClusterNode, int connectionTimeout,
                                int soTimeout, final GenericObjectPoolConfig poolConfig) {
            super(jedisClusterNode);
            super.connectionHandler = new JedisSlotAdvancedConnectionHandler(jedisClusterNode,
                    poolConfig, connectionTimeout, soTimeout);
        }

        public JedisSlotAdvancedConnectionHandler getConnectionHandler() {
            return (JedisSlotAdvancedConnectionHandler)this.connectionHandler;
        }

}
