package com.example.redis.pipeline;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IPipeline {
    public Map<JedisPool, List<String>> getJedisPoolKeys(Set<String> keys);

}
