package com.example.redis.pipeline;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Configuration
public class JedisClusterConfig {
    @Value("${spring.redis.cluster.nodes}")
    private String hostAndPort;
    Set<HostAndPort> hostAndPortSet = new HashSet<>();
    @Bean
    public JedisClusterPipeline jedisClusterPipeline(){
        return new JedisClusterPipeline(hostAndPortSet, 2000, 2000, new JedisPoolConfig());

    }


    @PostConstruct
    public void init(){
        String[] hps = hostAndPort.trim().split(",");
        for (int i = 0;i < hps.length; i++){
            String[] hp = hps[i].split(":");
            HostAndPort hap = new HostAndPort(hp[0],Integer.parseInt(hp[1]));
            hostAndPortSet.add(hap);
        }
    }
}
