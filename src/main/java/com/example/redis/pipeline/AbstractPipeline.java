package com.example.redis.pipeline;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.util.JedisClusterCRC16;

import javax.annotation.PostConstruct;
import java.util.*;

public abstract class AbstractPipeline implements IPipeline{

    @Autowired
    private JedisClusterPipeline jedisClusterPipeline;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public Map<JedisPool, List<String>> getJedisPoolKeys(Set<String> keys) {
        JedisSlotAdvancedConnectionHandler connectionHandler = jedisClusterPipeline.getConnectionHandler();
        Map<JedisPool,List<String>> jedisPoolListMap = new HashMap<>();
        keys.forEach(key->{
            int slot = JedisClusterCRC16.getSlot(key);
            JedisPool jedisPool = connectionHandler.getJedisPoolFromSlot(slot);
            if (jedisPoolListMap.keySet().contains(jedisPool)){
                List<String> keyList = jedisPoolListMap.get(jedisPool);
                keyList.add(key);
            }else {
                List<String> keyList = new ArrayList<>();
                keyList.add(key);
                jedisPoolListMap.put(jedisPool, keyList);
            }
        });
        return jedisPoolListMap;
    }
}
