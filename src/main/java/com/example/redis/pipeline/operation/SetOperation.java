package com.example.redis.pipeline.operation;
import com.example.redis.pipeline.AbstractPipeline;
import com.example.redis.pipeline.ISet;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;

import java.util.*;

@Component
public class SetOperation extends AbstractPipeline implements ISet {

    @Override
    public Set<Object> getSet(String key) {
        return null;
    }

    @Override
    public Long addSet(String key, Set<Object> values) {
        return null;
    }

    @Override
    public Long delSet(String key) {
        return null;
    }

    @Override
    public Long addSetBatch(Map<String, Set<String>> sets) {
        Map<JedisPool, List<String>> jedisPoolKeys = getJedisPoolKeys(sets.keySet());
        jedisPoolKeys.keySet().forEach(pool->{
            Jedis jedis = pool.getResource();
            Pipeline pipelined = jedis.pipelined();
            List<String> keys = jedisPoolKeys.get(pool);
            keys.forEach(key->{
                List<String> list = new ArrayList(sets.get(key));
                int size = list.size();
                final String[] values = new String[size];
                for (int i = 0; i < size;i++){
                    values[i] = list.get(i);
                }
                pipelined.sadd(key, values);
            });
            pipelined.sync();
            jedis.close();
        });
        return null;
    }
}
