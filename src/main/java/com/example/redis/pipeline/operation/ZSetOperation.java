package com.example.redis.pipeline.operation;

import com.example.redis.pipeline.AbstractPipeline;
import com.example.redis.pipeline.IZSet;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;

import java.util.List;
import java.util.Map;
import java.util.Set;
@Component
public class ZSetOperation extends AbstractPipeline implements IZSet {
    @Override
    public Set<ZSetOperations.TypedTuple<Object>> getZSet(String key) {
        return null;
    }

    @Override
    public int addZSet(String key, Set<ZSetOperations.TypedTuple<Object>> values) {
        return 0;
    }

    @Override
    public int delZSet(String key) {
        return 0;
    }

    @Override
    public int addZSetBatch(Map<String, Map<String, Double>> zSets) {
        Map<JedisPool, List<String>> jedisPoolKeys = getJedisPoolKeys(zSets.keySet());
        jedisPoolKeys.keySet().forEach(pool->{
            Jedis jedis = pool.getResource();
            Pipeline pipelined = jedis.pipelined();
            List<String> keys = jedisPoolKeys.get(pool);
            keys.forEach(key->{
                pipelined.zadd(key,zSets.get(key));
            });
            pipelined.sync();
            jedis.close();
        });
        return zSets.keySet().size();
    }
}
