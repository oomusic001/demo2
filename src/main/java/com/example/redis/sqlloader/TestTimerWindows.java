package com.example.redis.sqlloader;

import java.io.*;

public class TestTimerWindows {
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        // ./szrpp_files/clPath
        String path = System.getProperty("user.dir");
        //写控制文件.ctl
        String fileRoute = path+"\\test\\";//文件地址路径
        String fileName = "test2015.txt";//数据文件名
        String tableName = "test_table";//表名
        String fieldName = "(name,userpwd,sex)";//要写入表的字段
        String ctlfileName = "test_table.ctl";//控制文件名

        stlFileWriter(fileRoute,fileName,tableName,fieldName,ctlfileName);
        //要执行的DOS命令
        String user = "gueryang";
        String psw = "123456";
        String Database = "//127.0.1.1:1521/XE";
        //IP要指向数据库服务器的地址

        String logfileName = "test_table.log";
        Executive(user,psw,Database,fileRoute,ctlfileName,logfileName);

    }

    /**
     * * 写控制文件.ctl
     * @param fileRoute 数据文件地址路径
     * @param fileName 数据文件名
     * @param tableName 表名
     * @param fieldName 要写入表的字段
     * @param ctlfileName 控制文件名
     */
    public static void stlFileWriter(String fileRoute,String fileName,String tableName,String fieldName,String ctlfileName)
    {
        FileWriter fw = null;
        String strctl =
                "OPTIONS (skip=0)" +
                " LOAD DATA INFILE '"+fileRoute+""+fileName+"'" +
                " APPEND INTO TABLE "+tableName+"" +
                " FIELDS TERMINATED BY ','" +
                " OPTIONALLY  ENCLOSED BY \"'\"" +
                " TRAILING NULLCOLS "+fieldName+"";
        try {
            fw = new FileWriter(fileRoute+""+ctlfileName);
            fw.write(strctl);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally {
            try
            {
                fw.flush();
                fw.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

        }
    }

    /**
     * 调用系统DOS命令
     * @param user
     * @param psw
     * @param Database
     * @param fileRoute 文件路径
     * @param ctlfileName 控制文件名
     * @param logfileName 日志文件名
     */
    public static void Executive(String user,String psw,String Database,String fileRoute,String ctlfileName,String logfileName)
    {
        InputStream ins = null;
        //要执行的DOS命令  --数据库  用户名  密码  user/password@database
        String dos="sqlldr "+user+"/"+psw+"@"+Database+" control="+fileRoute+""+ctlfileName+" log="+fileRoute+""+logfileName;

        String[] cmd = new String[]
                { "cmd.exe", "/C", dos }; // 命令
        try
        {
            Process process = Runtime.getRuntime().exec(cmd);
            ins = process.getInputStream(); // 获取执行cmd命令后的信息

            BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
            String line = null;
            long start = System.currentTimeMillis();
            while ((line = reader.readLine()) != null)
            {
                String msg = new String(line.getBytes("ISO-8859-1"), "UTF-8");
                //System.out.println(msg); // 输出
            }
            int exitValue = process.waitFor();
            long end = System.currentTimeMillis();
            System.out.println(end - start);
            if(exitValue==0)
            {
                System.out.println("返回值：" + exitValue+"\n数据导入成功");

            }else
            {
                System.out.println("返回值：" + exitValue+"\n数据导入失败");

            }

            process.getOutputStream().close(); // 关闭
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
