package com.example.redis.mapper;

import com.example.redis.pojo.UserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {

    public int insertBatch(@Param("list") List<UserEntity> list);

    public int truncate();

    public int insertBatchNoSeq(@Param("list") List<UserEntity> list);

}
