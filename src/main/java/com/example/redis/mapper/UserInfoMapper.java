package com.example.redis.mapper;

import com.example.redis.pojo.UserEntity;
import com.example.redis.pojo.UserInfoEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserInfoMapper {

    public List<UserInfoEntity> select();

}
