package com.example.redis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@MapperScan("com.example.redis.mapper")
public class RedisApplication {

	public static void main(String[] args) {
		//1
		SpringApplication.run(RedisApplication.class, args);
		//2
		SpringApplication springApplication = new SpringApplication(RedisApplication.class);
		Map<String,Object> properties  = new HashMap<>();
		properties.put("server.port",0);
		springApplication.setDefaultProperties(properties);
		//3
		new SpringApplicationBuilder(RedisApplication.class).properties("server.port=0").run(args);

	}

}
