package com.example.redis.pk.pojo;

import java.util.Date;

public class TopicEntity {

    private String id;
    private String title;
    private String publishMan;
    private Date publishTime;
    private Date startTime;
    private Date endTime;
    private String supportTitle;
    private String opposeTitle;
    private String auditFlag;
    private String auditManId;
    private String auditManName;

    public String getId() {
        return id;
    }

    public TopicEntity setId(String id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public TopicEntity setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getPublishMan() {
        return publishMan;
    }

    public TopicEntity setPublishMan(String publishMan) {
        this.publishMan = publishMan;
        return this;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public TopicEntity setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
        return this;
    }

    public Date getStartTime() {
        return startTime;
    }

    public TopicEntity setStartTime(Date startTime) {
        this.startTime = startTime;
        return this;
    }

    public Date getEndTime() {
        return endTime;
    }

    public TopicEntity setEndTime(Date endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getSupportTitle() {
        return supportTitle;
    }

    public TopicEntity setSupportTitle(String supportTitle) {
        this.supportTitle = supportTitle;
        return this;
    }

    public String getOpposeTitle() {
        return opposeTitle;
    }

    public TopicEntity setOpposeTitle(String opposeTitle) {
        this.opposeTitle = opposeTitle;
        return this;
    }

    public String getAuditFlag() {
        return auditFlag;
    }

    public TopicEntity setAuditFlag(String auditFlag) {
        this.auditFlag = auditFlag;
        return this;
    }

    public String getAuditManId() {
        return auditManId;
    }

    public TopicEntity setAuditManId(String auditManId) {
        this.auditManId = auditManId;
        return this;
    }

    public String getAuditManName() {
        return auditManName;
    }

    public TopicEntity setAuditManName(String auditManName) {
        this.auditManName = auditManName;
        return this;
    }
}
