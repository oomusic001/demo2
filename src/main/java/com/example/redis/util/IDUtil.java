package com.example.redis.util;

public class IDUtil {


    //雪花算法：1位bit  + 41位时间戳 + 10位机器id  + 12位序列号  组成64位二进制。
    //64不是固定的，可以根据业务变更。
    //机器位可以根据使用机器的多少变更(2^10=1024台机器)，序列号可以根据业务的并发数变更(2^12=4096并发)。
    //雪花算法 手写： 0 + 41位时间戳 + 5位业务id + 7位机器id + 序列号


    /**
     * 1.得到每个部分的值
     * 2.先得到每个组成的部分长度
     * 3.套用公式
     * 4.如果是同一毫秒，序列号递增。
     */


    //1.得到每一个部分的值 ,系统开始时间
    private final  long startTime = System.currentTimeMillis();
    //业务id
    private  long businessType;
    //机器id
    private  long workId;
    //序列号 从0开始计算
    private long serialNum = 0L;

    //每个组成部分的长度  时间戳的长度不用定义，因为时间戳的唯一长度是根据其他长度计算的。
    private  final long serialNumBits = 10L;
    private  final long workIdBits = 7L;
    private  final long businessTypeBits = 5L;
    //上一毫秒的执行时间  ，如果从来没有执行 按0计算。
    private long lastTimeStamp = 0L;
    /**
     *   00000000 00000001     1的二进制
     *   11111111 11111110     1的二进制取反
     *   11111111 11111111     1的二进制取反 后补码，变为-1的二进制。
     *
     *   11111100 00000000    左位移1位    -1 << serialNumBits
     *   11111111 11111111
     *   ---------------------------------- 与-1 异或   相同为0 不同为1
     *   00000011 11111111     -1^(-1 << serialNumBits)     代表10位的二级制最大值。
     *
     */
    //序列号的最大长度.   -1^(-1 << serialNumBits)  代表  serialNumBits  位的二进制最大值(00000011 11111111)，这样效率高。
    private final long serialNumMax = -1^(-1 << serialNumBits);
    // 左位移长度
    private final long workIdShift = serialNumBits;
    private final long businessTypeShift = workIdShift + workIdBits;
    private final long timeStampShift = businessTypeShift +businessTypeBits;

    public IDUtil(long businessType, long workId) {
        this.businessType = businessType;
        this.workId = workId;
    }

    public synchronized long getId(){
        //当前系统时间
        long timeStamp = System.currentTimeMillis();
        //如果当前执行时间和上一毫秒执行时间一模一样，则说明序列号重复了。
        if (timeStamp == lastTimeStamp){
            //同一毫秒执行的，serialNum + 1；如果超过最大长度，等到下一毫秒执行，同时serialNum = 0；
            serialNum = (serialNum + 1) & serialNumMax;
            if (serialNum == 0){
                //超过最大值，等到下一毫秒执行
                timeStamp = waitNextMills(timeStamp);
            }
        }else {
            //serialNum = 0  ;//。 会导致id分布不均匀，如全是偶数。
            serialNum = timeStamp & 1; //todo   与 1 得到二进制的时间值 。timeStamp是随时变化的。
        }
        //把当前执行时间赋值给上一毫秒执行时间。
        lastTimeStamp = timeStamp;
        return ((timeStamp - startTime)<<timeStampShift)
                | (businessType << businessTypeShift)
                | (workId << workIdShift)
                | serialNum;
    }

    private long waitNextMills(long timeStamp) {
        long nowTimeStamp = System.currentTimeMillis();
        while (timeStamp>=nowTimeStamp){
            nowTimeStamp = System.currentTimeMillis();
        }
        return  nowTimeStamp;
    }

    public static void main(String[] args) throws InterruptedException {
        IDUtil test = new IDUtil(12, 5);
        for (int i = 0;i<10;i++){
            //Thread.sleep(10); //todo 有这个会导致全是偶数结尾的。导致分布不均匀。
            long id = test.getId();
            System.out.println(id);
        }
    }
}
