package com.example.redis.util;

import org.springframework.data.redis.core.ZSetOperations;

import java.util.*;

public class RedisUtil {
    private int random = (int)(Math.random() * 9 + 1);
    private Random ran = new Random(1000);

    /**
     * 模拟获取粉丝列表txt文件内容
     * @return
     */
    public String getIdsString(){
        IDUtil idUtil = new IDUtil(1,2);
        StringBuilder sb = new StringBuilder();
        String id = "";
        for (int i = 0;i<1000000;i++){
            //String id = idUtil.getId()+"y0y";
            id = UUID.randomUUID().toString();
            if (i%random == 0 || i%random == 2 ){
                sb.append(id + ",|");
            }else if (i%random == 1|| i%random == 3 ){
                sb.append(id + "," + id.replaceAll("1","2") + "|");
            }else {
                sb.append("," + id +"|");
            }
        }
        return sb.toString();
    }
    /**
     * 模拟获取活跃粉丝集合
     */
    public Set<Object> getActiveUserSet(){
        //将pid粉丝集合模拟为活跃客户
        return getIdSet().get("pidSet");
    }

    /**
     *
     * @return
     */
    public Set<Object> getFollowUserSet(){
        //将pid粉丝集合模拟为活跃客户
        return getIdSet().get("followSet");
    }
    /**
     * 模拟获取粉丝列表(包含所有粉丝集合，及只有pid粉丝集合)
     * @return
     */
    public Map<String,Set<Object>> getIdSet(){

        String idsStr = getIdsString();

        Set<Object> followSet = new HashSet<>();
        Set<Object> pidSet = new HashSet<>();
        Map<String,Set<Object>> hashMap = new HashMap<>();

        String[] idArr = idsStr.split("\\|");

        Arrays.stream(idArr).forEach(user->{
            String[] split = user.trim().split(",");
            if (user.startsWith(",")){
                followSet.add(split[1]);
            }else {
                followSet.add(split[0]);
                pidSet.add(split[0]);
            }
        });
        hashMap.put("pidSet",pidSet);
        hashMap.put("followSet",followSet);
        return hashMap;
    }
    public Map<String,Double> getPipelineMapZSetData(){
        Set<Object> followSet = getIdSet().get("followSet");
        Map<String,Double> hashMap = new HashMap<>();
        followSet.forEach(id->{
            hashMap.put(id.toString(),Double.parseDouble(System.currentTimeMillis() + ran.nextInt() + ""));
        });
        return hashMap;
    }
    /**
     * 模拟获取set数据
     */
    public Set<Object> getSetData(){
        return getIdSet().get("followSet");
    }
    /**
     * 模拟获取zset数据
     * @return
     */
    public Set<ZSetOperations.TypedTuple<Object>> getZSetData(){
        Set<ZSetOperations.TypedTuple<Object>> zsets = new HashSet<>();
        IDUtil idUtil = new IDUtil(1,2);
        getIdSet().get("followSet").forEach(id->{
            zsets.add(new ZSetOperations.TypedTuple<Object>() {
                @Override
                public int compareTo(ZSetOperations.TypedTuple<Object> o) {
                    return 0;
                }
                @Override
                public Object getValue() {
                    return id;
                }
                @Override
                public Double getScore() {
                    return Double.parseDouble(ran.nextInt()+"");
                }
            });
        });
        return zsets;
    }



}
