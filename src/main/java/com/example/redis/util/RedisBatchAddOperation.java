package com.example.redis.util;

import com.example.redis.thread.RedisBatchAddSetThread;
import com.example.redis.thread.RedisBatchAddZSetThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.template.TemplateLocation;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

import java.time.temporal.Temporal;
import java.util.*;
@Component
public class RedisBatchAddOperation {

    @Autowired
    private RedisTemplate redisTemplate;

    public void batchAddZSet(Set<ZSetOperations.TypedTuple<Object>> zsets ,long ttl,String key,int batchNum) throws InterruptedException {
        List<ZSetOperations.TypedTuple<Object>> list = new ArrayList<>(zsets);
        int size = zsets.size();
        int cpuCores = Runtime.getRuntime().availableProcessors();
        int batchN = size % batchNum == 0 ? size / batchNum : size / batchNum + 1;
        int threadNum = batchN < cpuCores ? batchN : cpuCores * 3;
        RedisBatchAddZSetThread batchAddZSetThread = null;
        List<ZSetOperations.TypedTuple<Object>> temp = null;
        int index = size / threadNum;
        Thread[] threads = new Thread[threadNum];
        long start = System.currentTimeMillis();
        for (int i = 0 ;i<threadNum;i++){
            if (i == threadNum - 1){
                temp = list;
            }else {
                temp = list.subList(0,index);
                list = list.subList(index,list.size());
            }
            //System.out.println(temp.get(0).getValue());
            batchAddZSetThread = new RedisBatchAddZSetThread(redisTemplate,  key,  batchNum,  ttl, temp);
            threads[i] = new Thread(batchAddZSetThread);
            threads[i].start();
        }

        long end = System.currentTimeMillis();
        System.out.println("多线程耗时："+(end - start));
    }

    /**
     * 批量入库 set
     * @param sets
     * @param ttl
     * @param key
     * @param batchNum
     * @throws InterruptedException
     */
    public void batchAddSet(Set<Object> sets ,long ttl,String key,int batchNum) throws InterruptedException {
        List<Object> list = new ArrayList<>(sets);
        int size = sets.size();
        int cpuCores = Runtime.getRuntime().availableProcessors();
        int batchN = size % batchNum == 0 ? size / batchNum : size / batchNum + 1;
        int threadNum = batchN < cpuCores ? batchN : cpuCores * 3;
        RedisBatchAddSetThread batchAddSetThread = null;
        List<Object> temp = null;
        int index = size / threadNum;
        Thread[] threads = new Thread[threadNum];
        long start = System.currentTimeMillis();
        for (int i = 0 ;i<threadNum;i++){
            if (i == threadNum - 1){
                temp = list;
            }else {
                temp = list.subList(0,index);
                list = list.subList(index,list.size());
            }
            //System.out.println(temp.get(0).getValue());
            batchAddSetThread = new RedisBatchAddSetThread(redisTemplate,  key,  batchNum,  ttl, temp);
            threads[i] = new Thread(batchAddSetThread);
            threads[i].start();
        }
        for (int i = 0 ;i< threads.length;i++){
            threads[i].join();
        }
        long end = System.currentTimeMillis();
        System.out.println("多线程耗时："+(end - start));
    }
}
