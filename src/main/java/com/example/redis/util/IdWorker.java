package com.example.redis.util;

public class IdWorker {
    // 机器id
    private long workerId;
    private long datacenterId;
    // 并发控制
    private long sequence;
    // 时间起始标记点，作为基准，一般取系统的最近时间（一旦确定不能变动）
    private long twepoch = System.currentTimeMillis();
    // 机器标识位数
    private long workerIdBits = 5L;
    // 数据中心标识位数
    private long datacenterIdBits = 5L;
    private long maxWorkerId = -1L ^ (-1L << workerIdBits);
    private long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);
    //序列长度
    private long sequenceBits = 12L;
    // 左位移长度
    private long workerIdShift = sequenceBits;
    private long datacenterIdShift = sequenceBits + workerIdBits;
    //
    private long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;
    //序列号的最大长度.   -1^(-1 << serialNumBits)  代表  serialNumBits  位的二进制最大值(00000011 11111111)，这样效率高。
    private long sequenceMask = -1L ^ (-1L << sequenceBits);

    private long lastTimestamp = -1L;

    /**
     * 构造方法
     * @param workerId
     * @param datacenterId
     * @param sequence
     */
    public IdWorker(long workerId, long datacenterId, long sequence){

        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0",maxWorkerId));
        }
        if (datacenterId > maxDatacenterId || datacenterId < 0) {
            throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0",maxDatacenterId));
        }
        System.out.printf("worker starting. timestamp left shift %d, datacenter id bits %d, worker id bits %d, sequence bits %d, workerid %d",
                timestampLeftShift, datacenterIdBits, workerIdBits, sequenceBits, workerId);

        this.workerId = workerId;
        this.datacenterId = datacenterId;
        this.sequence = sequence;
    }



    public long getWorkerId(){
        return workerId;
    }

    public long getDatacenterId(){
        return datacenterId;
    }

    public long getTimestamp(){
        return System.currentTimeMillis();
    }

    public synchronized long nextId() {
        //获取当前时间的时间戳
        long timestamp = timeGen();
        //校验始终回拨
        if (timestamp < lastTimestamp) {
            //如果发现时钟回拨，抛出异常。
            throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        }
        //并发情况下如果是在同一毫秒的请求，则序列号增加。
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) & sequenceMask;
            if (sequence == 0) {
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            sequence = 0;
        }
        //将本次请求的时间戳赋值给上一毫秒的执行时间戳
        lastTimestamp = timestamp;
        return ((timestamp - twepoch) << timestampLeftShift) |
                (datacenterId << datacenterIdShift) |
                (workerId << workerIdShift) |
                sequence;
    }

    private long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    private long timeGen(){
        return System.currentTimeMillis();
    }

    //---------------测试---------------
    public static void main(String[] args) {
        IdWorker worker = new IdWorker(1,1,3);
        for (int i = 0; i < 30; i++) {
            System.out.println(worker.nextId());
        }
    }
}
