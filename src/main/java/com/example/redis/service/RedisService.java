package com.example.redis.service;

import com.example.redis.util.IDUtil;
import com.example.redis.util.RedisUtil;
import com.google.common.collect.Sets;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

import java.util.*;

@Service
public class RedisService {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 模拟将百万数据放入redis
     */
    public void addZSetToRedis(String fun){
        RedisUtil util = new RedisUtil();
        Set<ZSetOperations.TypedTuple<Object>> zSetData = util.getZSetData();

        Object[] objects = zSetData.toArray();
        List<Object> objects1 = Arrays.asList(objects);
        System.out.println(objects1.get(0).getClass());


        Map<String, Double> pipelineMapZSetData = util.getPipelineMapZSetData();
        BoundZSetOperations zSetOperations = redisTemplate.boundZSetOps("active_user_" + fun);
        long start = System.currentTimeMillis();
        Long num   = zSetOperations.add(zSetData);
        long end   = System.currentTimeMillis();
        System.out.println(end - start);
        //使用pipeline的情况下
        Jedis jedis = new Jedis("192.168.206.30" , 6379);
        Pipeline pipeline = jedis.pipelined();
        pipeline.zadd("active_user_0888",pipelineMapZSetData);
        pipeline.syncAndReturnAll();
        long end2 = System.currentTimeMillis();
        System.out.println(end2 - end);

    }
    public Set getDiff(String srcKey,String newKey){
        SetOperations setOperations = redisTemplate.opsForSet();
        return setOperations.difference(srcKey, newKey);
    }





    public static void main(String[] args) {
        System.out.println("".length());
    }
}
