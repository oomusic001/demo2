package com.example.redis.service;

import com.example.redis.mapper.UserMapper;
import com.example.redis.pojo.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public int insertBatch(List<UserEntity> userList){
        return userMapper.insertBatch(userList);
    }
    public int truncate(){
        return userMapper.truncate();
    }
    public int insertBatchNoSeq(List<UserEntity> userList){
        return userMapper.insertBatchNoSeq(userList);
    }
}
