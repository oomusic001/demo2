package com.example.redis.thread;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RedisBatchAddSetThread implements Runnable{
    private RedisTemplate redisTemplate;
    private String key;
    private int batchNum;
    private long ttl;
    private List<Object> list;

    public RedisBatchAddSetThread(RedisTemplate redisTemplate, String key, int batchNum, long ttl, List<Object> list) {
        this.redisTemplate = redisTemplate;
        this.key = key;
        this.batchNum = batchNum;
        this.ttl = ttl;
        this.list = list;
    }

    @Override
    public void run() {
        int size = list.size();
        int num = size % batchNum == 0 ? size / batchNum : size /batchNum + 1;
            Object[] zset = null;
            for (int i = 0 ;i < num ;i++){
                try {
                    if (i == num -  1){
                        zset = list.toArray();
                    }else {
                        zset = list.subList(0,batchNum).toArray();
                        list = list.subList(batchNum,list.size());
                    }
                    redisTemplate.boundSetOps(key).add(zset);
                }catch (Exception e){
                    redisTemplate.boundSetOps(key).add(zset);
                    e.printStackTrace();
                }

            }
    }
}
