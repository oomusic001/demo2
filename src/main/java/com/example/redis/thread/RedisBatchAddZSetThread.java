package com.example.redis.thread;

import com.example.redis.pojo.UserEntity;
import org.springframework.data.redis.core.BoundZSetOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RedisBatchAddZSetThread implements Runnable{
    private RedisTemplate redisTemplate;
    private String key;
    private int batchNum;
    private long ttl;
    private List<ZSetOperations.TypedTuple<Object>> list;

    public RedisBatchAddZSetThread(RedisTemplate redisTemplate, String key, int batchNum, long ttl, List<ZSetOperations.TypedTuple<Object>> list) {
        this.redisTemplate = redisTemplate;
        this.key = key;
        this.batchNum = batchNum;
        this.ttl = ttl;
        this.list = list;
    }

    @Override
    public void run() {
        int size = list.size();
        int num = size % batchNum == 0 ? size / batchNum : size /batchNum + 1;
            Set<ZSetOperations.TypedTuple<Object>> zset = null;
            Long lon = 0L;
            Long add = 0L;
            for (int i = 0 ;i < num ;i++){
                try {
                    if (i == num -  1){
                        zset = new HashSet<>(list);
                    }else {
                        zset = new HashSet<>(list.subList(0,batchNum));
                        list = list.subList(batchNum,list.size());
                    }
                    add = redisTemplate.boundZSetOps(key).add(zset);
                    lon = lon + add;
                }catch (Exception e){
                    redisTemplate.boundZSetOps(key).add(zset);
                    lon = lon + add;
                    e.printStackTrace();
                }

            }
            System.out.println(lon+"========");
    }
}
