package com.example.redis.thread;

import com.example.redis.pojo.UserEntity;
import com.example.redis.pojo.UserInfoEntity;
import com.example.redis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BatchInsertThread implements Runnable {
    //单个线程负责入库的总数据
    private List<UserInfoEntity> list;
    //单个线程每次入库的数量
    private Integer batchNum;

    public BatchInsertThread(List<UserInfoEntity> list, int batchNum) {
        this.list = list;
        this.batchNum = batchNum;
    }

    /**
     * 开启线程，批量入库
     */
    @Override
    public void run() {
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:XE","gueryang","123456");
            //String sql = "insert INTO  active_user2 values (?,?)";
            String sql = "insert into userinfo values (myseq.nextval,?,?,?,sysdate)";
            statement = conn.prepareStatement(sql);
            conn.setAutoCommit(false);
            //单个线程负责入库的总数据大小。
            int size = list.size();
            //单个线程入库的总次数。
            int num = (size - 1) / batchNum + 1;
            //单个线程每次入库的实际数据。
            List<UserInfoEntity> dbData = null;
            //切分数据索引
            int start ,end;
            for (int n = 0; n < num; n++){
                start = n*batchNum;
                end   = n == (num - 1) ? list.size() : (n+1)*batchNum;
                dbData = list.subList(start,end);
                for (int i = 0; i < dbData.size(); i++){
                    statement.setString(1,dbData.get(i).getSid());
                    statement.setString(2,dbData.get(i).getPid().substring(0,12));
                    statement.setString(3,dbData.get(i).getUserid());
                    statement.addBatch();
                }
                statement.executeBatch();
                conn.commit();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            try {
                if (statement != null){
                    statement.close();
                }
                if (conn != null){
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
}
