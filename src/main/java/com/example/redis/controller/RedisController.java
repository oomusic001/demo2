package com.example.redis.controller;

import com.example.redis.pipeline.JedisSlotAdvancedConnectionHandler;

import com.example.redis.pipeline.PipelineTemplate;
import com.example.redis.pipeline.operation.SetOperation;
import com.example.redis.pipeline.operation.ZSetOperation;
import com.example.redis.service.RedisService;
import com.example.redis.util.RedisBatchAddOperation;
import com.example.redis.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.*;
import redis.clients.jedis.util.JedisClusterCRC16;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class RedisController {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedisService redisService;
    @Autowired
    private PipelineTemplate pipelineTemplate;
    @Autowired
    private ZSetOperation zSetOperation;
    @Autowired
    private SetOperation setOperation;
    @Autowired
    RedisBatchAddOperation batchAddOperation;
    @RequestMapping("redis")
    @ResponseBody
    public String redis() throws InterruptedException {
        Properties properties = System.getProperties();
        Map<String, String> getenv = System.getenv();
        String active = System.getenv("active");
        String active1 = System.getProperty("active");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar calendar = new GregorianCalendar();
        Date now = new Date();
        calendar.setTime(now);
        calendar.add(5,-7);
        String time = format.format(calendar.getTime());
        System.out.println(time);


        long l = System.currentTimeMillis();
        System.out.println(l);
        Map<String, Map<String, Double>> zSets = new HashMap<>();
        for (int i = 0;i<1000;i++){
            Map<String,Double> members = new HashMap<>();
            members.put("小明："+i,90D);
            members.put("小华："+i,80D);
            zSets.put("班级："+i,members);
        }
        zSetOperation.addZSetBatch(zSets);

        Map<String,Set<String>> setMap = new HashMap<>();

        for (int i = 0;i < 1000000;i++){
            Set<String> set = new HashSet<>();
            set.add("set1");
            set.add("set2");
            setMap.put("set:"+i,set);
        }
        setOperation.addSetBatch(setMap);
        return "ok";
    }


}
