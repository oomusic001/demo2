package com.example.redis.controller;

import com.example.redis.mapper.UserInfoMapper;
import com.example.redis.mapper.UserMapper;
import com.example.redis.pojo.UserEntity;
import com.example.redis.pojo.UserInfoEntity;
import com.example.redis.service.RedisService;
import com.example.redis.service.UserService;
import com.example.redis.thread.BatchInsertThread;
import com.example.redis.util.RedisUtil;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    RedisUtil util = new RedisUtil();
    @RequestMapping("user")
    @ResponseBody
    public String user() throws InterruptedException {
        long starts = System.currentTimeMillis();
        List<UserInfoEntity> select = userInfoMapper.select();
        System.out.println(System.currentTimeMillis() - starts);

        //入库的数据
        List<UserInfoEntity> userList = new ArrayList<>();
        Set<Object> userSet = util.getActiveUserSet();
        Iterator<Object> iterator = userSet.iterator();
        while (iterator.hasNext()){
            userList.add(new UserInfoEntity()
                    .setSid("225522522212121")
                    .setPid(iterator.next().toString())
                    .setUserid("454545454121"));
        }
        //入库的总数据大小
        int total = userList.size();
        //total = totalN;       //todo total
        //每批次入库限制最大条数
        int batchMaxNum = 20000; //todo num
        //预估分批次数
        int batchNum = total % batchMaxNum == 0 ? total / batchMaxNum : total / batchMaxNum + 1;
        //获取cpu核心数
        int cpuCores = Runtime.getRuntime().availableProcessors();
        //创建的线程数量(也是实际分批次数)
        int threadNum = cpuCores > batchNum ? batchNum : cpuCores*3;
        //threadNum = threadN;   //todo threadN
        //分批截取索引
        int start ,end;
        //每个线程负责入库的平均大小
        int aveNum = total / threadNum;
        //数据分批,即将需要入库的总数据分成threadNum份。
        List<List<UserInfoEntity>> batchData = new ArrayList();
        for (int i = 0; i < threadNum; i++){
            start = i*aveNum;
            end   = i == (threadNum - 1) ? total : (i+1)*aveNum;
            batchData.add(userList.subList(start,end));
        }

        long start1 = System.currentTimeMillis();
        //线程数组
        BatchInsertThread[] threads = new BatchInsertThread[threadNum];
        Thread[] threadArr = new Thread[threadNum];
        //创建线程，插入数据
        for (int i = 0; i < batchData.size(); i++){
                threads[i] = new BatchInsertThread(batchData.get(i),batchMaxNum);
                threadArr[i] = new Thread(threads[i]);
                threadArr[i].start();
                System.out.println("开启线程："+i);
        }
        /*for (int i = 0;i<threadArr.length;i++){
            threadArr[i].join();
        }*/
        long mon = System.currentTimeMillis();
        System.out.println("多线程耗时："+(mon - start1));
        return "ok";
    }
    @RequestMapping("truncate")
    @ResponseBody
    public String truncate(){
        int truncate = userService.truncate();
        return truncate + "";
    }
}
