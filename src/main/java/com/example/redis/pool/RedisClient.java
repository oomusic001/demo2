//package com.example.redis.pool;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.params.SetParams;
//
//public class RedisClient {
//        @Autowired
//        private JedisPool jedisPool;
//
//        public void set(String key, Object value) {
//            Jedis jedis = null;
//            try {
//                jedis = jedisPool.getResource();
//                jedis.set(key, value.toString());
//            }catch (Exception e){
//                if (jedis != null)
//                    jedis.close();
//                e.printStackTrace();
//            }finally {
//                if (jedis != null)
//                    jedis.close();
//            }
//        }
//
//        /**
//         * 设置过期时间
//         * @param key
//         * @param value
//         * @param exptime
//         * @throws Exception
//         */
//        public void setWithExpireTime(String key, String value, int exptime) {
//            Jedis jedis = null;
//            try {
//                jedis = jedisPool.getResource();
//                jedis.set(key, value, new SetParams());
//            } catch (Exception e){
//                if (jedis != null)
//                    jedis.close();
//                e.printStackTrace();
//            }finally {
//                if (jedis != null)
//                    jedis.close();
//            }
//        }
//
//        public String get(String key) {
//
//            Jedis jedis = null;
//            try {
//                jedis = jedisPool.getResource();
//                return jedis.get(key);
//            } catch (Exception e){
//                if (jedis != null)
//                    jedis.close();
//                e.printStackTrace();
//            }finally {
//                if (jedis != null)
//                    jedis.close();
//            }
//            return null;
//        }
//        public JedisPool getJedisPool() {
//            return jedisPool;
//        }
//
//        public void setJedisPool(JedisPool jedisPool) {
//            this.jedisPool = jedisPool;
//        }
//
//}
