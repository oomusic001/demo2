package com.example.redis.pojo;

public class UserEntity {
    private String userId;
    private long activeTime;

    public String getUserId() {
        return userId;
    }

    public UserEntity setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public long getActiveTime() {
        return activeTime;
    }

    public UserEntity setActiveTime(long activeTime) {
        this.activeTime = activeTime;
        return this;
    }
}
