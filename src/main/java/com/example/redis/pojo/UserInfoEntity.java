package com.example.redis.pojo;

import java.util.Date;

public class UserInfoEntity {
    private int id;
    private String pid;
    private String userid;
    private Date atime;
    private String sid;

    public int getId() {
        return id;
    }

    public UserInfoEntity setId(int id) {
        this.id = id;
        return this;
    }

    public String getPid() {
        return pid;
    }

    public UserInfoEntity setPid(String pid) {
        this.pid = pid;
        return this;
    }

    public String getUserid() {
        return userid;
    }

    public UserInfoEntity setUserid(String userid) {
        this.userid = userid;
        return this;
    }

    public Date getAtime() {
        return atime;
    }

    public UserInfoEntity setAtime(Date atime) {
        this.atime = atime;
        return this;
    }

    public String getSid() {
        return sid;
    }

    public UserInfoEntity setSid(String sid) {
        this.sid = sid;
        return this;
    }
}
