package com.example.lock;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {


    public static void main(String[] args) {
        ConcurrentHashMap map = new ConcurrentHashMap();
        map.put("uuuu","iiiii");
        //aqs state = 3;
        CountDownLatch count = new CountDownLatch(3);

        new Thread(()->{
            //线程要完成的任务
            //...........
            //...........
            count.countDown();
        }).start();
        new Thread(()->{
            //线程要完成的任务
            //...........
            //...........
            count.countDown();
        }).start();
        new Thread(()->{
            //线程要完成的任务
            //...........
            //...........
            count.countDown();
        }).start();

        try {
            count.await(); //阻塞，当3 变为0 的时候释放。
            //所有的线程完成后，执行后面的代码
            //........
            System.out.println("执行完了。");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
