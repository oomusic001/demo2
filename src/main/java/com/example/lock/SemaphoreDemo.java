package com.example.lock;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {
    /**
     * 令牌。
     * @param args
     */
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(5);
        for (int i = 0;i < 10;i++){
            new DoAnything(i,semaphore).start();
        }
    }

    static class DoAnything extends  Thread{
        private int num;
        private Semaphore semaphore;


        public DoAnything(int num, Semaphore semaphore) {
            this.num = num;
            this.semaphore = semaphore;
        }

        //限流
        @Override
        public void run() {
            try {
                // 释放之前只能有五个线程获取令牌，其他先要想进来，只能等待其他线程释放令牌后。
                semaphore.acquire();//获取令牌
                System.out.println("第" + num +"个线程进入");
                Thread.sleep(10000);
                semaphore.release();//释放令牌
                System.out.println("第" + num +"个令牌释放");
            }catch (Exception e){

            }
        }
    }
}
