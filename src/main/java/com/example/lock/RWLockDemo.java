package com.example.lock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RWLockDemo {
    //  读 - 读共享
    // 读 - 写不共享，
    // 写 - 写不共享
    static Map<String,Object> cacheMap = new HashMap<>();
    static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    static Lock readLock = readWriteLock.readLock();
    static Lock writeLock = readWriteLock.writeLock();
    public static final Object get(String key){
        readLock.lock();
        Object o = cacheMap.get(key);
        readLock.unlock();
        return o;
    }



}
