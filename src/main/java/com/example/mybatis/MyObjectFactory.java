package com.example.mybatis;

import com.example.redis.pojo.UserEntity;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;

public class MyObjectFactory extends DefaultObjectFactory {

    @Override
    public Object create(Class type) {
        if (type .equals(UserEntity.class)){
            UserEntity obj = (UserEntity)super.create(type);
            obj.setUserId("00000");
            return obj;
        }
        return super.create(type);
    }
}
