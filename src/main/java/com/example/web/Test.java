package com.example.web;

import java.io.FileNotFoundException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public class Test {
    public static void main(String[] args){
        Class<?> clazz = FileNotFoundException.class;
        while (clazz != Throwable.class){
            System.out.println(clazz.getName());
            clazz = clazz.getSuperclass();
        }

        System.out.println(clazz.getName());
    }
}
