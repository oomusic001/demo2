package com.example.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MyConsumer {

    private final static String EXCHANGE_NAME = "SIMPLE_EXCHANGE";

    private final static String QUEUE_NAME = "SIMPLE_QUEUE";

    public static void main(String[] args) throws InterruptedException {
        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost("192.168.206.10");

        factory.setPort(5672);

        factory.setVirtualHost("/");

        factory.setUsername("root");
        factory.setPassword("123456");
        try {
            Connection connection = factory.newConnection();

            Channel channel = connection.createChannel();
            //声明交换机
            channel.exchangeDeclare(EXCHANGE_NAME,"direct",false,false,null);
            //声明队列
            channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            //队列绑定交换机
            channel.queueBind(QUEUE_NAME,EXCHANGE_NAME,"gupao.test");
            Consumer consumer = new DefaultConsumer(channel) {

                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body,"UTF-8");
                    System.out.println("received message"+message);
                    System.out.println("consumerTag"+consumerTag);
                    System.out.println("deliveryTag"+envelope.getDeliveryTag());

                }
            };
            //开始获取消息

            channel.basicConsume(QUEUE_NAME,true,consumer);

            channel.close();

            connection.close();
            Thread.sleep(90000);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

}
