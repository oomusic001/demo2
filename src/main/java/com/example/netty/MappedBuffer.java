package com.example.netty;

import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class MappedBuffer {
    public static void main(String[] args) throws Exception{
        RandomAccessFile raf = new RandomAccessFile("e://test.txt","rw");

        FileChannel fc = raf.getChannel();

        MappedByteBuffer map = fc.map(FileChannel.MapMode.READ_WRITE, 0, 26);

        map.put(0,(byte)97);
        map.put(3,(byte)122);
        fc.close();


    }

}
