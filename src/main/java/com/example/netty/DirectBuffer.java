package com.example.netty;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class DirectBuffer {
    public static void main(String[] args) {


        try {
            //首先从磁盘上读取到写出的文件内容
            String inFile = "e://test.txt";
            FileInputStream fis = new FileInputStream(inFile);
            FileChannel in = fis.getChannel();

            //把刚刚读取的内容写入到一个新的文件中。
            String outFile = String.format("e://testcopy.txt");
            FileOutputStream fos = new FileOutputStream(outFile);
            FileChannel out = fos.getChannel();

            //使用allocateDirect ,而不是allocate
            ByteBuffer buffer = ByteBuffer.allocateDirect(1024);
            while (true){
                buffer.clear();
                int r = in.read(buffer);
                if (r == -1){
                    break;
                }
                buffer.flip();
                out.write(buffer);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
