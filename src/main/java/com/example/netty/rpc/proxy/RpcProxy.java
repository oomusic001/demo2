package com.example.netty.rpc.proxy;

import com.example.netty.rpc.protocol.InvokerProtocol;
import com.example.netty.rpc.registry.RegistryHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class RpcProxy {
    public static<T> T create(Class<?> clazz){
        T result  = (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, new MethodProxy(clazz));
        return result;
    }

    /**
     * 通过代理的形式，将本地调用变为远程调用。
     */
    private static class MethodProxy implements InvocationHandler{
        private Class<?> clazz;

        public MethodProxy(Class<?> clazz) {
            this.clazz = clazz;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (Object.class.equals(method.getDeclaringClass())){//传入的是实现类，无需实现类，直接返回即可。
                return method.invoke(this,args);
            }
            return rpcInvoker(proxy,method,args);
        }

        private Object rpcInvoker(Object proxy, Method method, Object[] args) throws InterruptedException {
            //首先构建一个协议内容，消息
            InvokerProtocol msg = new InvokerProtocol();
            msg.setClassName(this.clazz.getName());
            msg.setMethodName(method.getName());
            msg.setParams(method.getParameterTypes());
            msg.setValues(args);

            final RpcProxyHandler proxyHandler = new RpcProxyHandler();

            //发起网络请求
            EventLoopGroup workGroup = new NioEventLoopGroup();

            Bootstrap client = new Bootstrap();
            //只有一个线程组
            client.group(workGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY,true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            //这个pipeline需要自己去实现，对逻辑的封装。有一定的执行顺序。下面两个是还原出InvokerProtocol对象
                            //通过自定义协议内容进行编解码。protocol协议。
                            pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE,0,4,0,4));
                            //自定义编码器
                            pipeline.addLast(new LengthFieldPrepender(4));
                            //实参处理   还原出InvokerProtocol 中的object对象   反序列化java能否识别的对象。
                            pipeline.addLast("encoder",new ObjectEncoder());
                            pipeline.addLast("decoder",new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));//每次都重新解析，不要缓存
                            // 前面的编解码就是完成对数据的解析
                            //最后一步，执行属于自己的逻辑。
                            //1.对传过来的对象进行注册，起名字。 对外提供服务的名字。
                            //2.服务的位置做一个登记。
                            pipeline.addLast(proxyHandler);
                        }
                    });
            ChannelFuture future = client.connect("localhost", 8085).sync();
            future.channel().writeAndFlush(msg).sync();
            future.channel().closeFuture().sync();
            workGroup.shutdownGracefully();
            return proxyHandler.getResponse();
        }
    }
}
