package com.example.netty.rpc.protocol;

import java.io.Serializable;

/**
 * 协议 ，根据该协议就可以找到要调用的方法
 */
public class InvokerProtocol implements Serializable {

    private String className;//服务名称

    private String methodName;//方法名称

    private Class<?> [] params;//形参列表

    private Object[] values;//实参列表

    public String getClassName() {
        return className;
    }

    public InvokerProtocol setClassName(String className) {
        this.className = className;
        return this;
    }

    public String getMethodName() {
        return methodName;
    }

    public InvokerProtocol setMethodName(String methodName) {
        this.methodName = methodName;
        return this;
    }

    public Class<?>[] getParams() {
        return params;
    }

    public InvokerProtocol setParams(Class<?>[] params) {
        this.params = params;
        return this;
    }

    public Object[] getValues() {
        return values;
    }

    public InvokerProtocol setValues(Object[] values) {
        this.values = values;
        return this;
    }
}
