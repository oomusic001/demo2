package com.example.netty.rpc.registry;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

public class RpcRegistry {

    private int port;

    public RpcRegistry(int port) {
        this.port = port;
    }
    public void start() throws InterruptedException {
        //主线程池初始化，selector
        EventLoopGroup boosGroup = new NioEventLoopGroup();
        //子线程池初始化，具体的对应客户端处理逻辑
        EventLoopGroup workerGroup = new NioEventLoopGroup();


        ServerBootstrap server = new ServerBootstrap();
        try {
            server.group(boosGroup,workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            //成功获取数据的实现
                            //当客户端连接过来的时候会触发这个方法。
                            //在netty，把所有的业务逻辑处理，归总到一个队列中。队列中包含了各种各样的处理逻辑。
                            //对这些处理逻辑在netty中有一个封装，封装为一个对象，封装为一个无锁化串行任务队列
                            //这个对象就是pipeline。
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            //这个pipeline需要自己去实现，对逻辑的封装。有一定的执行顺序。下面两个是还原出InvokerProtocol对象
                            //通过自定义协议内容进行编解码。protocol协议。
                            pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE,0,4,0,4));
                            //自定义编码器
                            pipeline.addLast(new LengthFieldPrepender(4));
                            //实参处理   还原出InvokerProtocol 中的object对象   反序列化java能否识别的对象。
                            pipeline.addLast("encoder",new ObjectEncoder());
                            pipeline.addLast("decoder",new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));//每次都重新解析，不要缓存
                            // 前面的编解码就是完成对数据的解析
                            //最后一步，执行属于自己的逻辑。
                            //1.对传过来的对象进行注册，起名字。 对外提供服务的名字。
                            //2.服务的位置做一个登记。
                            pipeline.addLast(new RegistryHandler());


                        }
                    }).option(ChannelOption.SO_BACKLOG,128)//指定一个最大的selectorkey的数量。
                    .childOption(ChannelOption.SO_KEEPALIVE,true)//每一个子线程保持一个长连接，可以在线程池中回收利用
            ;
            //正式启动服务，相当于用一个死循环开始轮询。
            ChannelFuture future = server.bind(this.port).sync();
            future.channel().closeFuture().sync();
            System.out.println("99999");
        }catch (Exception e){

        }finally {
            boosGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }


    }

    public static void main(String[] args) throws Exception{
        new RpcRegistry(8085).start();
    }
}
