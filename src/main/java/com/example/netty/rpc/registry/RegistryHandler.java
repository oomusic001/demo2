package com.example.netty.rpc.registry;

import com.example.netty.rpc.protocol.InvokerProtocol;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import sun.awt.image.ShortInterleavedRaster;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RegistryHandler extends ChannelInboundHandlerAdapter{







    public RegistryHandler() {
        //有客户端建立连接的时候，就会回调。
        //1. 根据包名，将所有的符合条件的class全部扫描出来，放到容器中。
        scannerClass("com.example.netty.rpc.provider");
        //2. 给每一个对应的class起一个唯一的名字，作为服务名称，保存到一个容器中。
        doRegistry();


    }
    private Map<String,Object> registryMap = new ConcurrentHashMap<>();
    /**
     * 注册
     */
    private void doRegistry() {
        if (classNames.isEmpty()) return;
        for (String className : classNames) {
            try {
                Class<?> clazz = Class.forName(className);
                Class<?> i = clazz.getInterfaces()[0];
                String serviceName = i.getName();
                //本来这里存的应该是网络的路径，从配置文件中读取，
                //在调用的时候再去解析。这里直接用反射调用。
                registryMap.put(serviceName,clazz.newInstance());
            }catch (Exception e){

            }

        }


    }
    private List<String> classNames = new ArrayList<>();
    //扫描
    //正常来说应该是读取配置文件，这里直接扫描本地class
    private void scannerClass(String packageName) {
        URL url = this.getClass().getClassLoader().getResource(packageName.replaceAll("\\.","/"));
        File classPath = new File(url.getFile());
        for (File file : classPath.listFiles()) {
            if (file.isDirectory()){
                scannerClass(packageName + "." + file.getName());
            }else {
                classNames.add(packageName + "." + file.getName().replace(".class",""));
            }

        }

    }
    //3.当有客户端连接后，netty会自动把信息解析为一个具体的对象，通过netty的编解码器，解析为一个对象InvokerProtocol，获取协议内容

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        InvokerProtocol request = (InvokerProtocol) msg;
        //4.要去注册好的容器中找到符合条件服务。

        Object result = new Object();
        if (registryMap.containsKey(request.getClassName())){
            Object service = registryMap.get(request.getClassName());
            Method method = service.getClass().getMethod(request.getMethodName(), request.getParams());
            result = method.invoke(service, request.getValues());
        }

        //5.通过远程调用provider得到返回结果，并回复给客户端。
        ctx.write(result);
        ctx.flush();
        ctx.close();

    }
    //连接发生异常的时候回调
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {


    }
}
