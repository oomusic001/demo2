package com.example.netty.rpc.consumer;

import com.example.netty.rpc.api.IRpcHelloService;
import com.example.netty.rpc.api.IRpcService;
import com.example.netty.rpc.provider.RpcHelloServiceImpl;
import com.example.netty.rpc.provider.RpcServiceImpl;
import com.example.netty.rpc.proxy.RpcProxy;

public class RpcConsumer {
    public static void main(String[] args) {




        int i = 1023;
        boolean b = (i & -i) == i;

        //本地调用
        //IRpcService service = new RpcServiceImpl();
        //IRpcHelloService helloService = new RpcHelloServiceImpl();

        //远程调用9
        IRpcHelloService helloService = RpcProxy.create(IRpcHelloService.class);
        IRpcService service = RpcProxy.create(IRpcService.class);
        for (int j = 0;j<100000;j++){
            System.out.println(helloService);
            System.out.println(helloService.hello("小明"));
            System.out.println( "8 + 2 = " + service.add(8,2));
        }


    }
}
