package com.example.netty.rpc.api;

public interface IRpcHelloService {

    public String hello(String name);

}
