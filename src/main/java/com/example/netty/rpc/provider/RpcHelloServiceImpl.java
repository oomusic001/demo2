package com.example.netty.rpc.provider;

import com.example.netty.rpc.api.IRpcHelloService;

public class RpcHelloServiceImpl implements IRpcHelloService {

    public String hello(String name){
        return "hello: "+name;

    }

}
