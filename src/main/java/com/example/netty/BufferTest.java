package com.example.netty;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class BufferTest {

    public static void main(String[] args) {
        try {
            FileInputStream is = new FileInputStream("e://test.txt");
            FileChannel channel = is.getChannel();
            //分配一个10个大小缓冲区，说白了就是分配一个10个大小的byte数组
            ByteBuffer buffer = ByteBuffer.allocate(10);
            output("初始化",buffer);
            //position是可操作的位置。  limit是最大可操作范围
            //先读一下
            channel.read(buffer);
            output("调用read()",buffer);

            //准备操作之前，先锁定操作范围
            buffer.flip();
            output("调用flip()",buffer);

            //判断有没有可读数据
            while (buffer.remaining() > 0){
                byte b = buffer.get();

            }
            output("调用get()",buffer);

            //可以理解为解锁
            buffer.clear();
            output("调用clear()",buffer);

            //最后把管道关闭
            is.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void output(String step, ByteBuffer buffer) {
        
        System.out.println(step + ":");
        //容量，数组大小
        System.out.print("capacity: " + buffer.capacity() + ",");
        
        //当前操作数据所在位置，也可以叫做游标
        System.out.print("position: " + buffer.position() + ",");
        
        //锁定值，flip，数据操作范围索引只能在position - limit之间
        System.out.print("limit:" + buffer.limit());
        System.out.println();
        
        
    }
    /**
     *  position: 被写入或者读取的元素索引，值由get()/put()自动更新。
     *  limit： 指定还有多少数据需要去除（在缓冲区写入通道时。）或者还有多少空间可以放入数据（在从通道读入缓存区时）
     *          可读或可写的  限制大小，当读时，表示的是0-limit 可读。写时，
     *  capacity： 缓冲区中的最大数据容量
     *
     *  关系： 0 <= position <= limit <= capacity
     *
     */

}
